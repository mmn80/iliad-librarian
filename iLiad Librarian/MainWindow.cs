using System;
using System.IO;
using System.Xml;
using Gtk;

namespace iLiadLibrarian
{
	public partial class MainWindow: Gtk.Window
	{	
		public MainWindow (): base (Gtk.WindowType.Toplevel)
		{
			Build ();
			filechooserbutton2.SetFilename(Properties.Settings.Default.Folder);
		}
		
		protected void OnDeleteEvent (object sender, DeleteEventArgs a)
		{
			Properties.Settings.Default.Folder = filechooserbutton2.Filename;
            Properties.Settings.Default.Save();
			Application.Quit ();
			a.RetVal = true;
		}

		protected virtual void OnButtonClick (object sender, System.EventArgs e)
		{
			messages.Buffer.Text = "";
			char sep = System.IO.Path.DirectorySeparatorChar;
            if (Directory.Exists(filechooserbutton2.Filename)) 
            {
                foreach (string directory in Directory.GetDirectories(filechooserbutton2.Filename))
                {
                    string manifest = directory + sep + "manifest.xml";
                    if (File.Exists(manifest))
                    {
                        XmlDocument doc = new XmlDocument();
                        try
                        {
                            doc.Load(manifest);
                            XmlNode title = doc.SelectSingleNode("/package/metadata/dc-metadata/Title");
                            XmlNode description = doc.SelectSingleNode("/package/metadata/dc-metadata/Description");
                            if (title == null || description == null)
                                messages.Buffer.Text += "ERROR: Either Title or Description tag not found in " + manifest + "\r\n\r\n";
                            else
                            {
                                if (String.IsNullOrEmpty(description.InnerText))
                                {
                                    string desc = GetDescription(title.InnerText);
                                    description.InnerText = desc;
                                    doc.Save(manifest);
                                    messages.Buffer.Text += manifest + " processed OK.\r\n\r\n";
                                }
                                else messages.Buffer.Text += "SKIP: Description already set to \"" + description.InnerText + "\" in " + manifest + "\r\n\r\n";
                            }
                        }
                        catch (Exception ex)
                        {
                            messages.Buffer.Text += "ERROR: Error processing " + manifest + ": " + ex.Message + "\r\n\r\n";
                        }
                    }
                    else messages.Buffer.Text += "SKIP: " + manifest + " not found, skiping...\r\n\r\n";
                }
                foreach (string pdf in Directory.GetFiles(filechooserbutton2.Filename, "*.pdf"))
                {
                    string dir = pdf + ".new";
                    string fileName = System.IO.Path.GetFileName(pdf);
                    try
                    {
                        string desc = GetDescription(fileName);
                        Directory.CreateDirectory(dir);
                        File.Move(pdf, dir + sep + fileName);
                        File.WriteAllText(dir + sep + "manifest.xml", @"<?xml version=""1.0"" encoding=""utf-8""?>
<package>
  <metadata>
    <dc-metadata>
      <Title>" + fileName + @"</Title>
      <Description>" + desc + @"</Description>
      <Date>" + DateTime.Now.ToString("yyyy-mm-ddThh:m:ss") + @"</Date>
    </dc-metadata>
    <y-metadata>
      <startpage>" + fileName + @"</startpage>
      <version>000</version>
    </y-metadata>
  </metadata>
  <last-location>
    <pagenumber>1</pagenumber>
  </last-location>
  <viewer-settings>
    <zoomfactor>159.064327</zoomfactor>
    <rotation>0</rotation>
    <positionx>108</positionx>
    <positiony>-5</positiony>
    <mode>continous</mode>
  </viewer-settings>
</package>");
                        Directory.Move(dir, pdf);
                        messages.Buffer.Text += "OK: Single file " + pdf + " processed OK\r\nDescription set to: " + desc + "\r\n\r\n";
                    }
                    catch (Exception ex)
                    {
                        messages.Buffer.Text += "ERROR: Error processing single file " + pdf + ": " + ex.Message + "\r\n\r\n";
                    } 
                }
            }
            else
                messages.Buffer.Text = "ERROR: Select a valid directory.\r";
		}
		
		private string GetDescription(string fileName)
        {
            string desc = fileName;
            if (desc.EndsWith(".pdf"))
                desc = desc.Substring(0, desc.Length - 4);
            if (desc.StartsWith("["))
                desc = desc.Substring(6);
            desc = desc.Trim();
            if (ckRemoveAuthors.Active == true)
            {
                int p = desc.IndexOf("- ");
                if (p > 0)
                    desc = desc.Substring(p + 2);
            }
            return desc;
        }
	}
}